package com.trongtran.Controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trongtran.entity.Employee;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {
    final String URL_GETALL = "http://localhost:8080/RestFulServer/rest/employee/getall";
    final String URL_ADD = "http://localhost:8080/RestFulServer/rest/employee/add/{id}/{name}";
    final String URL_UPDATE = "http://localhost:8080/RestFulServer/rest/employee/update/{id}/{name}";

    @RequestMapping("/")
    public String home(ModelMap map){
        RestTemplate restTemplate = new RestTemplate();
        String string = restTemplate.getForObject(URL_GETALL, String.class);

        ObjectMapper mapper = new ObjectMapper();
        List<Employee> result = new ArrayList<Employee>();
        try {
            result = result = mapper.readValue(string,new TypeReference<List<Employee>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        map.addAttribute("employees",result);
        return "index";
    }

    @RequestMapping("/addview")
    public String addView(){
        return "addemployee";
    }

    @RequestMapping("/updateview")
    public String updateView(){
        return "updateemployee";
    }

    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public String add(ModelMap map,@RequestParam("id") String id, @RequestParam("name") String name){
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(URL_ADD, String.class,id,name);
        return "redirect:/";
    }


    @RequestMapping(value = "/update",method = RequestMethod.PUT)
    public String update(ModelMap map,@RequestParam("id") String id, @RequestParam("name") String name){
        RestTemplate restTemplate = new RestTemplate();
        String result= restTemplate.getForObject(URL_UPDATE, String.class,id,name);
        return "redirect:/";
    }
}
